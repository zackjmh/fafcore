#include <FAFcore/Fork.h>
#include <FAFcore/Logger.h>

int main()
{
  FAF::Logging::ColorLog("Starting Fork test", FAF::Logging::Colors::GREEN);
  auto test = FAF::System::Fork{[](){}};

  test.Wait();
  FAF::Logging::ColorLog("%d", FAF::Logging::Colors::GREEN, test.ReturnValue());

  if(test.ReturnValue() == FAF::System::Fork::FAF_UNKNOWN_EXIT)
  {
    FAF::Logging::ColorLog("Yay", FAF::Logging::Colors::GREEN);
    return 0;
  }
  else
  {
    FAF::Logging::ErrorLog("Boo");
    return 1;
  }
}
