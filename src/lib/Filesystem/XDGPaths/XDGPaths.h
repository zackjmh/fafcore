#pragma once
#include <FAFcore/Logger.h>
#include <FAFcore/Variables.h>
#include <FAFcore/FunctionWrappers.h>

namespace FAF::Filesystem
{
  //TODO: Put this somewhere else, just need these functor generators here
  inline auto Concator(const std::string& lhs, const std::string& rhs)
  {
    return [=](){ return lhs + rhs; };
  }


  inline std::vector<File> TokenizeToFiles(std::string target, char delim)
  {
    auto nextToken = [](auto& target, char delim)
    {
      auto index = target.find(delim);
      if(index != std::string::npos)
      {
        auto out = target.substr(0, index - 1);
        target = target.substr(index + 1);
        return out;
      }
      else
      {
        auto out = target;
        target = "";
        return out;
      } 
    };

    std::vector<File> out{};

    while(!target.empty())
    {
      out.emplace_back(nextToken(target, delim));
    }

    return out;
  }

  //TODO: This needs to be generalized for non POSIX systems (i.e. Windows)
  class XDGPaths
  {
  public:
    static const XDGPaths& Variables()
    {
      static XDGPaths paths{};
      return paths;
    }

    std::string GetDataHome() const
    {
      return m_dataHome.Path();
    }

  private:
    XDGPaths() = default;

    File m_dataHome{GetLifetimeXdgVar("XDG_DATA_HOME", "/.local/share/")};
    File m_configHome{GetLifetimeXdgVar("XDG_CONFIG_HOME", "/.config/")};
    File m_cacheHome{GetLifetimeXdgVar("XDG_CACHE_HOME", "/.cache/")};
    File m_runtimeDir{System::Variables::GetVariableOr("XDG_RUNTIME_DIR", []() { return "";})};

    //TODO: Implement these
    std::vector<File> m_dataDirs{GetXdgSystemDirs("XDG_DATA_DIRS", "/usr/local/share:/usr/share")};
    std::vector<File> m_configDirs{GetXdgSystemDirs("XDG_CONFIG_DIRS", "/etc/xdg")};

    static File GetLifetimeXdgVar(const std::string& xdgVar, const std::string& defaultLoc)
    {
      return File{System::Variables::GetVariableOr(xdgVar, Concator(System::Variables::GetVariableOr("HOME", Functional::Identor("./")), defaultLoc))};
    }

    static std::vector<File> GetXdgSystemDirs(const std::string& xdgVar, const std::string& defaultDirs)
    {
      return TokenizeToFiles(System::Variables::GetVariableOr(xdgVar, Functional::Identor(defaultDirs)), ':');
    }
  };
}
