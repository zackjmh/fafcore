#include <FAFcore/Directory.h>

#include <FAFcore/File.h>

namespace FAF::Filesystem
{
  File::File(std::filesystem::directory_entry&& info) :
    m_fileInfo(std::forward<std::filesystem::directory_entry>(info))
  {
  }

  File::File(std::string_view path) :
    File(std::filesystem::directory_entry{std::move(path)})
  {
  }

  File File::Create(const std::string_view& fileName, const std::string& defaultText)
  {
    File newFile{fileName};

    newFile.AsWriteableStream() << defaultText;
    return newFile;
  }

  std::string File::Path() const
  {
    return m_fileInfo.path().string();
  }

  std::string File::Name() const
  {
    return m_fileInfo.path().stem().string();
  }

  std::string File::Ext() const
  {
    return m_fileInfo.path().extension().string();
  }

  std::string File::Fullname() const
  {
    return m_fileInfo.path().filename().string();
  }

  File File::Location() const
  {
    return File{m_fileInfo.path().parent_path().string()};
  }

  bool File::CachedExists() const
  {
    return m_fileInfo.exists();
  }

  bool File::Exists() const
  {
    const_cast<File*>(this)->m_fileInfo.refresh();

    return CachedExists(); 
  }

  std::ofstream File::AsWriteableStream() const
  {
    return std::ofstream{Path()};
  }

  std::filesystem::file_time_type File::Timestamp() const
  {
    if(!Exists())
    {
      return {};
    }

    return m_fileInfo.last_write_time();
  }

  File File::LocatedAt(const File& newLoc) const
  {
    return LocatedAt(newLoc.Path());
  }

  File File::BasedAt(const File& newBase) const
  {
    return BasedAt(newBase.Path());
  }

  File File::WithExt(const std::string_view& newExt) const
  {
    auto tempPath = m_fileInfo.path();

    tempPath.replace_extension(newExt);

    return File{tempPath.string()};
  }

  File File::LocatedAt(const std::string_view& newLoc) const
  {
    auto tempPath = std::filesystem::path{newLoc.data() + Fullname()};

    return File{tempPath.string()};
  }

  File File::BasedAt(const std::string_view& newBase) const
  {
    auto tempPath = std::filesystem::path{newBase.data() + m_fileInfo.path().string()};
    return File{tempPath.string()};
  }
  
  size_t File::Size() const
  {
    using filesize_t = decltype(m_fileInfo.file_size());

    try
    {
      auto out = m_fileInfo.file_size();
      return out;
    }
    catch(std::exception& e)
    {
      //TODO: Error logging
      return filesize_t{0};
    }
  }

  void File::ArchiveAndReplace(const File& destFile)
  {
    auto archiveFile = File{destFile.Location().Path() + "/.archive/" + destFile.Path()};
    Directory::EnsureExists(archiveFile.Location());

    destFile.EnsureCopyTo(archiveFile);
    EnsureCopyTo(destFile);
  }
  
}
