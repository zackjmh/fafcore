#include "Directory.h"

namespace FAF::Filesystem
{

  ExistingDirectory::ExistingDirectory(std::string path) :
    m_root{std::filesystem::directory_entry(std::move(path))}
  {
    for(auto file : std::filesystem::directory_iterator(m_root.Path()))
    {
      if(file.is_directory() && file.path() != m_root.Path())
      {
        m_subdirs.emplace_back(file.path());
      }
      else
      {
        m_files.emplace_back(std::move(file));
      }
    }
  }

  std::optional<ExistingDirectory::file_t> ExistingDirectory::LookupFilename(const std::string& toFind) const
  {
    std::optional<file_t> out{};

    if(m_root.Name() == toFind)
    {
      out.emplace(m_root);
      return out;
    }

    for(auto&& file : m_files)
    {
      if(file.Name() == toFind)
      {
        out.emplace(file);
        return out;
      }
    };

    for(auto&& subdir : m_subdirs)
    {
      out = subdir.LookupFilename(toFind);

      if(out)
      {
        return out;
      }
    }

    return out;
  }

  void Directory::EnsureExists(const std::string& path)
  {
    try
    {
      std::filesystem::create_directories(path);
    }
    catch(std::exception& e)
    {
      Logging::ErrorLog("Failed to ensure directory %s existed with error %s", path, e.what());
      throw;
    }
  }

  Collections::Vector<ExistingDirectory::file_t> ExistingDirectory::CollectFilesByExt(const std::string_view& ext) const
  {
    return CollectFilesIf([&ext](auto&& file) { return ext == file.Ext(); });
  }

  std::string Directory::WorkingDir()
  {
    return std::filesystem::current_path();
  }
}
