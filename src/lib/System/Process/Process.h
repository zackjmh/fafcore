#pragma once

#include <functional>
#include <type_traits>

#include <unistd.h>
#include <sys/wait.h>


#include <FAFcore/Fork.h>
#include <FAFcore/Pipe.h>
#include <FAFcore/Vector.h>

namespace FAF::System
{
  template<typename T>
  constexpr auto GetString(T&& val)
  {
    if constexpr(std::is_same_v<std::decay_t<T>, char const*>)
    {
      return val;
    }
    else
    {
      return val.c_str();
    }
  }

  class Process
  {
  public:
    template<typename...Args>
    Process(const std::string& program, Args&&... args) :
      m_program{program}
    {
      Fork();

      if(IsChild())
      {
        execlp(m_program.c_str(), m_program.c_str(), GetString(args)..., NULL);
        exit(125);
      }
    }

    Process(const std::string& program, const std::vector<std::string>& args);

    static constexpr auto NO_HANDLER = [](){};
    
    template<typename ErrorHandler = decltype(NO_HANDLER)>
    static void CreateAndWait(const std::string& fileName, const std::vector<std::string>& args, ErrorHandler onErr = NO_HANDLER)
    {
      auto process = Process(fileName, args);
      if(auto ret = process.ReturnValue(); ret != 0)
      {
        process.LogErrors();

        onErr();

	      std::string argStr{};
	      for(auto&& arg : args)
	      {
          argStr += arg + " ";
	      }
	      
        throw std::runtime_error("Process " + fileName + " with args " + argStr + " returned " + std::to_string(ret));
      }
    }

    int ReturnValue();

    void LogErrors();

    void LogOutput();

    bool IsChild() const;

    bool IsExited();

  private:
    Pipe m_stdOut{};
    Pipe m_stdErr{};
    std::string m_program;

    ssize_t m_pid{};
    int32_t m_stats{};

    void Fork();

    void EnsureNotChild() const;

    void UpdateStats();
  };
}
