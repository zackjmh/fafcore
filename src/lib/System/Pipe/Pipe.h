#pragma once

#include <array>
#include <string>

namespace FAF::System
{

  class Pipe
  {
  public:
    Pipe();
    
    ~Pipe();
      
    void WriteTo(int handle);
   
    std::string Read();
   
    void CloseWrite();
      
    void CloseRead();
   
    void CloseAll();
  private:
    std::array<int, 2> m_pipe{};

    std::string m_pipeOutput{};

    void OpenPipe();  
  };

}
