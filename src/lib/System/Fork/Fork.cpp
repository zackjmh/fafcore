#include <cassert>
#include <unistd.h>
#include <sys/wait.h>
#include "Fork.h"
namespace FAF::System
{
  
  bool Fork::IsChild() const
  {
    return m_pid == 0;
  }

  void Fork::Wait()
  {
    assert(!IsChild() && "Do not wait on the parent process");

    waitpid(m_pid, &m_stats, 0);
  }

  int32_t Fork::ReturnValue() const
  {
    assert(!IsChild() && "Do not call return value from the child");

    if(WIFEXITED(m_stats))
    {
      return WEXITSTATUS(m_stats);
    }
    else
    {
      throw std::runtime_error("Child hasn't returned yet");
    }
  }
}
