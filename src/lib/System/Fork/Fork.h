#pragma once

#include <array>
#include <string>
#include <string_view>
#include <FAFcore/Pipe.h>
#include <unistd.h>

namespace FAF::System
{
  
  class Fork
  {
  public:
    static constexpr int32_t FAF_UNKNOWN_EXIT = 125;

    template<typename Func, typename...Args>
    Fork(Func&& func, Args&&... args)
    {
      if(IsChild())
      {
        func(std::forward<Args>(args)...);
        exit(FAF_UNKNOWN_EXIT);
      }
    }
 
    bool IsChild() const;

    void Wait();

    int32_t ReturnValue() const;

  private:
    ssize_t m_pid{fork()};
    int m_stats{};
  };

}
