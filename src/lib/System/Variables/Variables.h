#pragma once
#include <cstdlib>
namespace FAF::System
{
  class Variables
  {
  public:
    static constexpr auto NULL_STR = [](){return "";};

    template<typename OrFunctor = decltype(NULL_STR)>
    static std::string GetVariableOr(const std::string& envVarKey, OrFunctor&& func = NULL_STR)
    {
      auto envVar = std::getenv(envVarKey.c_str());
      
      return envVar != nullptr ? envVar : func();
    }
  };
}
