#pragma once
#include <functional>

namespace FAF::Functional
{
  template<typename T>
  auto Identor(const T& t)
  {
    return [=]() { return t; };
  }

  template<typename T>
  auto Identor()
  {
    return [](auto&& thing) { return thing; };
  }
  
  template<typename Func, typename This, typename...Args>
  auto MemberCall(Func&& function, This&& target, Args&&...args)
  {
    return (target.*function)(std::forward<Args>(args)...);
  }

  template<typename FirstArg, typename...Args>
  auto PeelFirst(FirstArg&& first, Args&&...args)
  {
    return std::forward<FirstArg>(first);
  }

  template<typename Func, typename...Args>
  auto Call(Func&& function, Args&&...args)
  {
    if constexpr(std::is_member_function_pointer_v<std::decay_t<Func>>)
    {
      return MemberCall(std::forward<Func>(function), std::forward<Args>(args)...);
    }
    else
    {
      return function(std::forward<Args>(args)...);
    }
  }

  template<typename Func>
  auto MaybeCall(Func&& func)
  {
    if constexpr(std::is_invocable_v<Func>)
    {
      return func();
    }
    else
    {
      return func;
    }
  }

  template<typename Func, typename...Args>
  auto PartialApply(Func&& function, Args&&...args)
  {
    return [&function, &args...](auto&&...maybeMore)
    {
        if constexpr(std::is_invocable_v<Func, Args..., decltype(maybeMore)...>)
        {
            return Call(std::forward<Func>(function), std::forward<Args>(args)..., maybeMore...);
        }
        else
        {
            return PartialApply(std::forward<Func>(function), std::forward<Args>(args)..., maybeMore...);
        }
    };
  }

  template<typename Func, typename Curried, typename...Args>
  auto Ladel(Func&& function, Curried&& curried, Args&&...args);

  template<typename Func, typename...Args>
  auto CurryRecurse(Func&& function, Args&&...args)
  {
    if constexpr(sizeof...(Args) == 0)
    {
        return MaybeCall(function);
    }
    else
    {
        return Ladel(std::forward<Func>(function), std::forward<Args>(args)...);
    }
  }

  template<typename Func, typename...Args>
  auto Curry(Func&& function, Args&&...args)
  {
    auto PartialApplyer = [](auto&&...why)
    {
        return PartialApply(why...);
    };
    
    return CurryRecurse(PartialApplyer, std::forward<Func>(function), std::forward<Args>(args)...);
  }

// vodo code from https://stackoverflow.com/questions/16337610/how-to-know-if-a-type-is-a-specialization-of-stdvector
template<typename Test, template<typename...> class Ref>
struct is_specialization : std::false_type {};

template<template<typename...> class Ref, typename... Args>
struct is_specialization<Ref<Args...>, Ref>: std::true_type {};

//template<template<typename...> class Ref, typename... Args>
//using is_speciazlization_v = is_specialization<Ref, Args...>::value;

  template<typename Func, typename Tuple, size_t...I, typename...Args>
  auto Unpack(Func&& function, Tuple&& tuple, std::index_sequence<I...>, Args&&...args)
  {
    return Call(std::forward<Func>(function), std::forward<Args>(args)..., std::get<I>(tuple)...);
  }

  template<typename Func, typename Curried, typename...Args>
  auto Ladel(Func&& function, Curried&& curried, Args&&...args)
  {
    if constexpr(is_specialization<std::decay_t<Curried>, std::tuple>::value)
    {
        static constexpr auto TUPLE_SIZE = std::tuple_size<std::decay_t<Curried>>::value;

        return CurryRecurse(Unpack(std::forward<Func>(function), std::forward<Curried>(curried), std::make_index_sequence<TUPLE_SIZE>{}), std::forward<Args>(args)...);
    }
    else if constexpr(std::is_same_v<std::invoke_result_t<decltype(Call<Func, Curried>), Func, Curried>, void>)
    {
      // TODO: Allow extra args to pass through?
      static_assert(sizeof...(Args) == 0, "Error, extra arguments passed to function");
      Call(std::forward<Func>(function), std::forward<Curried>(curried));
    }
    else
    {
        return CurryRecurse(Call(std::forward<Func>(function), std::forward<Curried>(curried)), std::forward<Args>(args)...);
    }
  }

  template<typename Func, typename This, typename...Args>
  auto OnThis(Func&& memberFunc, This&& callOn, Args&&...args)
  {
    return [&callOn, &memberFunc, &args...](auto&&...additionalArgs)
    {
      return Curry(std::forward<Func>(memberFunc), std::forward<This>(callOn), additionalArgs..., std::forward<Args>(args)...);
    };
  }
  
  template<typename Func, typename...Args>
  constexpr bool IsCallable()
  {
    return std::is_invocable_v<Func, Args...>;
  }

  template<typename Func, typename...Args>
  constexpr auto Wrap(Func&& function, Args&&...args)
  {
    if constexpr (IsCallable<Func>() && sizeof...(args) == 0)
    {
      return function;
    }
    else
    {
      return [&function, &args...]() { return Curry(std::forward<Func>(function), std::forward<Args>(args)...); };
    }
  }



  template<typename Func, typename...Args>
  auto Passthrough(Func&& function, Args&&...args)
  {
    return [&function, &args...](auto&& passthroughArg)
    {
      static_assert(std::is_invocable_v<decltype(Curry<Func, decltype(passthroughArg), Args...>), Func, decltype(passthroughArg), Args...>, "Error: Passthrough on function not invocable");

      if constexpr (false && std::is_same_v<std::invoke_result_t<decltype(Curry<Func, decltype(passthroughArg), Args...>), Func, decltype(passthroughArg), Args...>, void>)
      {
        Curry(std::forward<Func>(function), passthroughArg, std::forward<Args>(args)...);
        return passthroughArg;
      }
      else
      {
        return std::make_tuple(passthroughArg, Curry(std::forward<Func>(function), passthroughArg, std::forward<Args>(args)...));
      }
    };
  }

  template<typename Func, typename...Args>
  auto AlphaAndOmega(Func&& function, Args&&...args)
  {
    return [&function, &args...](auto&&... toDup)
    {
      return MaybeCall(Curry(std::forward<Func>(function), toDup..., std::forward<Args>(args)..., toDup...));
    };
  }

  template<typename Pred>
  constexpr bool Predicate(Pred&& predicate)
  {
    if constexpr(std::is_convertible_v<std::decay_t<Pred>, bool>)
    {
      return predicate;
    }
    else if constexpr(std::is_convertible_v<decltype(std::declval<std::decay_t<Pred>>()()), bool>)
    {
      return predicate();
    }
  }

  template<typename Pred, typename Func, typename...Args>
  auto FunctionIf(Pred&& predicate, Func&& function, Args&&...args)
  {
    using ReturnType = std::invoke_result_t<Func, Args...>;
    using OutputType = std::optional<std::function<ReturnType()>>;

    if(Predicate(std::forward<Pred>(predicate)))
    {
      return OutputType{Wrap(std::forward<Func>(function), std::forward<Args>(args)...)};
    }
    else
    {
      return OutputType{std::nullopt};
    }
  }

  template<typename Return = void>
  class Chain
  {
  public:
    template<typename Pred, typename Func, typename...Args>
    Chain(Pred&& run, Func&& function, Args&&...args)
      : m_maybeFunction{FunctionIf(std::forward<Pred>(run), std::forward<Func>(function), std::forward<Args>(args)...)}{}

    ~Chain() noexcept(false)
    {
      (*this)();
    }


    Return operator()()
    {
      auto toRun = m_maybeFunction.value_or([](){});

      m_maybeFunction.reset();
      
      return toRun();
    };

    template<typename Pred, typename Func, typename...Args>
    Chain& ElseIf(Pred&& pred, Func&& function, Args&&...args)
    { 
      m_maybeFunction.emplace(m_maybeFunction.value_or(*FunctionIf(pred, std::forward<Func>(function), std::forward<Args>(args)...)));
      return *this;
    }    

    template<typename Func, typename...Args>
    Chain& Else(Func&& func, Args&&... args)
    {
      m_maybeFunction.emplace(m_maybeFunction.value_or(Wrap(std::forward<Func>(func), std::forward<Args>(args)...)));
      return *this;
    }

  private:
    // TODO: Ech, make this something better than the heavyweight std::function
    std::optional<std::function<Return()>> m_maybeFunction{};
  };


  template<typename Pred, typename Func, typename...Args>
  auto DoIf(Pred&& predicate, Func&& function, Args&&...args)
  {
    return Chain{std::forward<Pred>(predicate), std::forward<Func>(function), std::forward<Args>(args)...};
  }
}
