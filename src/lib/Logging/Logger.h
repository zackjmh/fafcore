#pragma once

#include <string>
#include <FAFcore/File.h>

namespace FAF::Logging
{

  enum class Colors
  {
    WHITE,
    RED,
    GREEN,
    YELLOW,
    BLUE,
    MAGENTA,
    CYAN
  };
  
  class ColoredSegment
  {
  public:
    ColoredSegment(const std::string& segment, Colors color = Colors::WHITE) :
      m_segment{ColorString(color) + segment + ColorString(Colors::WHITE)}
    {}

    const std::string& operator()() const
    {
      return m_segment;
    }

  private:
    std::string m_segment;

    static constexpr const char * ColorString(Colors color)
    {
      constexpr auto WHITE = "\x1B[0m";
      constexpr auto RED = "\x1B[31m";
      constexpr auto GREEN = "\x1B[32m";
      constexpr auto YELLOW = "\x1B[33m";
      constexpr auto BLUE = "\x1B[34m";
      constexpr auto MAGENTA = "\x1B[35m";
      constexpr auto CYAN = "\x1B[36m";

      switch(color)
      {
        case Colors::RED:
          return RED;
        case Colors::GREEN:
          return GREEN;
        case Colors::YELLOW:
          return YELLOW;
        case Colors::BLUE:
          return BLUE;
        case Colors::MAGENTA:
          return MAGENTA;
        case Colors::CYAN:
          return CYAN;
        case Colors::WHITE:
        default:
          return WHITE;
      }
    };

  };

  

  template<typename T>
  auto Unpack(T&& arg)
  {
    if constexpr(std::is_same_v<std::decay_t<T>, std::string>)
    {
      return arg.c_str();
    }
    else if constexpr(std::is_base_of_v<Filesystem::File, std::decay_t<T>>)
    {
      return arg.Path();
    }
    else if constexpr(std::is_same_v<std::decay_t<T>, std::string_view>)
    {
      return arg.data();
    }
    else if constexpr(std::is_convertible_v<const char*, T> || std::is_same_v<const char*, T>)
    {
      return arg;
    }
    else if constexpr(std::is_constructible_v<std::string, T>)
    {
      return std::string{std::forward<T>(arg)};
    }
    else
    {
      // printf args should match
      return arg;
    }
  }

  template<typename...Args>
  void ColorLog(const std::string& fmt, Colors color, Args&&... args)
  {
    auto coloredString = ColoredSegment(fmt, color);

    printf((coloredString() + '\n').c_str(), Unpack(Unpack(args))...); 
  }


  template<typename...Args>
  void ErrorLog(const std::string& fmt, Colors color, Args&&...args)
  {
    auto coloredString = ColoredSegment(fmt, color);

    fprintf(stderr, (coloredString() + '\n').c_str(), Unpack(Unpack(args))...);
  }

  template<typename...Args>
  void ErrorLog(const std::string& fmt, Args&&...args)
  {
    ErrorLog(fmt, Colors::RED, std::forward<Args>(args)...);
  }

}

