#pragma once
#include <vector>
#include <future>

#include <FAFcore/FunctionWrappers.h>
#include <FAFcore/Logger.h>

namespace FAF::Async
{
  template<typename Future>
  class ParallelDispatcher
  {
  public:
    using return_t = Future;

    template<typename> friend class ParallelDispatcher;

    template<typename Target, typename Func, typename...Args>
    ParallelDispatcher(const std::vector<Target>& targets, Func&& function, Args&&...args)
    {
      m_futs.reserve(targets.size());
      for(auto& ele : targets)
      {
        m_futs.emplace_back(std::async(std::launch::async, std::forward<Func>(function), ele, std::forward<Args>(args)...));
      }
    }


    template<typename Target, typename Func, typename...Args>
    ParallelDispatcher(std::vector<Target>&& targets, Func&& function, Args&&...args)
    {
      m_futs.reserve(targets.size());
      for(auto& ele : targets)
      {
        m_futs.emplace_back(std::async(std::launch::async, std::forward<Func>(function), std::move(ele), std::forward<Args>(args)...));
      }
    }

    template<typename Other, typename Func, typename...Args>
    ParallelDispatcher(ParallelDispatcher<Other>& other, Func&& func, Args&&...args)
    {
      auto WhenReady = [](auto&& func, auto&& fut, auto&&...args)
      {
        return Functional::Curry(func, fut.get(), args...);
      };

      m_futs.reserve(other.m_futs.size());
      for(auto& fut : other.m_futs)
      {
        m_futs.emplace_back(std::async(std::launch::async, WhenReady, std::forward<Func>(func), std::move(fut), std::forward<Args>(args)...));
      }
    }

    ~ParallelDispatcher(){}

    static constexpr auto NO_HANDLER = [](){};

    template<typename ThenFunc, typename...Args>
    auto Then(ThenFunc&& thenFunc, Args...args)
    {
      return ParallelDispatcher<std::invoke_result_t<ThenFunc, return_t, Args...>>{*this, std::forward<ThenFunc>(thenFunc), std::forward<Args>(args)...};
    }

    template<typename Func = decltype(NO_HANDLER), typename...Args>
    auto Wait(const std::string& errStr = "", Func function = NO_HANDLER, Args&&...args)
    {
      std::exception_ptr err;
      for(auto&& fut : m_futs)
      {
        try
        {
          if constexpr(std::is_same_v<Func, decltype(NO_HANDLER)>)
          {
            (void)function;
            fut.get();
          }
          else
          {
            Functional::Call(std::forward<Func>(function), std::move(fut.get()), std::forward<Args>(args)...);
          }
        }
        catch(std::exception& e)
        {
          Logging::ErrorLog(errStr + " %s", e.what());
          err = std::current_exception();
        }
      }

      m_futs.clear();

      if(err)
      {
        std::rethrow_exception(err);
      }
    }

  private:
    ParallelDispatcher(){};

    std::vector<std::future<return_t>> m_futs{};
  };

}
