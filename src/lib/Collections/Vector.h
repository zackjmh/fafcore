#pragma once
#include <vector>
#include <FAFcore/FunctionWrappers.h>

namespace FAF::Collections
{
  // Currently just std::vector with extra utilities
  // May in the future replace that on platforms with poor stl implementations
  template<typename T>
  class Vector : public std::vector<T>
  {
  public:
    using contained_t = T;

    template<typename Collection>
    void Append(const Collection& toAppend)
    {
      AppendReserve(toAppend);
      this->insert(this->end(), toAppend.begin(), toAppend.end());
    }

    template<typename Collection>
    void Append(Collection&& toMoveAppend)
    {
      AppendReserve(toMoveAppend);
      this->insert(this->end(), std::make_move_iterator(toMoveAppend.begin()), std::make_move_iterator(toMoveAppend.end()));
    }

    template<typename Collection, typename Func, typename...Args>
    void ApplyAndAppend(const Collection& toAppend, Func&& func, Args&&...args)
    {
      AppendReserve(toAppend);

      for(auto&& ele : toAppend)
      {
        this->emplace_back(Functional::Call(std::forward<Func>(func), ele, std::forward<Args>(args)...));
      }
    }

    template<typename...Args>
    void EmplaceBack(Args&&...args)
    {
      this->emplace_back(std::forward<Args>(args)...);
    }

    const std::vector<T>& AsVector()
    {
      return *this;
    }

  private:
    template<typename Collection>
    void AppendReserve(const Collection& other)
    {
      this->reserve(this->size() + other.size());
    }
  };

  template<typename T>
  class StaticVector
  {
  public:

  private:
  };
}
