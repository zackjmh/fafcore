#pragma once
#include <string_view>
#include <FAFcore/Logger.h>
#include <FAFcore/File.h>

using SplitString = std::pair<std::string_view, std::string_view>;

const auto ThrowHandler = [](auto&& input) { throw std::runtime_error("No delim found on line: " + std::string{input});  return SplitString{}; };


template<typename ElseFunc = decltype(ThrowHandler), typename...Args>
inline SplitString Split(const std::string_view& input, char delim, ElseFunc handlerFunc = ThrowHandler, Args&&...extraHandlerArgs)
{
  auto delimPos = input.find(delim);

  if(delimPos != std::string::npos)
  {
    const auto key = input.substr(0, delimPos);
    const auto payload = input.substr(delimPos + 1);
    return std::pair{std::move(key), std::move(payload)};
  }
  else
  {
    return handlerFunc(input, std::forward<Args>(extraHandlerArgs)...);
  }
}

template<template<typename,typename...> typename Container = std::vector>
inline Container<std::string> Tokenize(const std::string_view& input, char delim)
{
  Container<std::string> out{};
  std::string_view remaining{input};
  std::string_view token{};

  do
  {
    std::tie(token, remaining) = Split(remaining, delim, [](auto&& line) { return SplitString{line, ""}; });
    out.emplace_back(token);
  } while(!remaining.empty());

  return out;
}
namespace FAF::Config
{

  class ConfigFile : public Filesystem::File
  {
  public:

    ConfigFile(const std::string_view& fileName)
      : Filesystem::File{fileName}
    {
      Logging::ErrorLog("Loading config file: %s", fileName);
    };

    ConfigFile(const Filesystem::File& configFile)
      : Filesystem::File{configFile}{};

  protected:
    template<typename Keymapable>
    void Register(Keymapable& toRegister)
    {
      m_setters.emplace(Keymapable::KEY, toRegister); 
    }

    void Load()
    {
      std::ifstream configFile{Path()};
      std::string line;
    
      while(configFile >> std::ws >> line >> std::ws)
      {
        try
        {
          auto [key, payload] = Split(line, '=');
          // In theory payload should always be null-terminated, but doing it the view way anyway since it is one
          FAF::Logging::ColorLog("Mapping var at key %.*s to %.*s", FAF::Logging::Colors::MAGENTA, key.size(), key, payload.size(), payload);
          if(m_setters.count(key))
          {
            m_setters.at(key).Set(payload);
          }
          else
          {
            throw std::runtime_error("Key not recognized: " + std::string{key});
          }
        }
        catch(std::exception& e)
        {
          FAF::Logging::ErrorLog("Failed to map line: %s", e.what());
        }
      }
    }
  

  private:
    class SetEraser
    {
    public:
      template<typename T>
      SetEraser(T& toTarget)
        : m_target{&toTarget},
          m_vtable{&vtable_for<std::decay_t<T>>}{}

      void Set(const std::string_view& toSet)
      {
        m_vtable->Set(m_target, toSet);
      }

    private:
      void* m_target;
      struct vtable
      {
        void(*Set)(void* target, const std::string_view& toSet);
      };

      template<typename T>
      static inline vtable const vtable_for = 
      {
        [](void* target, const std::string_view& toSet)
        {
          static_cast<T*>(target)->Set(toSet);
        },
      };

      vtable const *const m_vtable;
    };
    std::unordered_map<std::string_view, SetEraser> m_setters{};
  };

#ifndef CONFIG_MACROS
#define CONFIG_MACROS

#define CONFIG_FILE(FILE_NAME)                                      \
  class FILE_NAME : public Config::ConfigFile                       \
  {                                                                 \
  private:                                                          \
    using CurrentFile = FILE_NAME;                                  \
  public:                                                           \
    FILE_NAME () : ConfigFile(#FILE_NAME){ Load();};                \
                                                                    \
    FILE_NAME (const Filesystem::File& remoteFile)                  \
      : ConfigFile{remoteFile}                                      \
    {                                                               \
      if(remoteFile.Fullname() != #FILE_NAME)                       \
      {                                                             \
        throw std::runtime_error("Target file has incorrect name"); \
      }                                                             \
      Load();                                                       \
    };                                                              \
    void Reload(const std::string_view& newFile)                    \
    {                                                               \
      UpdatePath(newFile);                                          \
      Load();                                                       \
    }

#define END_CONFIG_FILE() \
  };

#define MAGIC_VALUE(KEY, DEFAULT_VALUE)             \
  private:                                          \
    using KEY##Type = decltype(DEFAULT_VALUE);      \
    const KEY##Type m_##KEY = DEFAULT_VALUE;        \
  public:                                           \
    const KEY##Type& KEY () const                   \
    {                                               \
      return m_##KEY;                               \
    }

#define NEW_KEYMAPPED_VAR(KEY_VAL, DEFAULT_VALUE)                                 \
  private:                                                                        \
    struct KEY_VAL##Map                                                           \
    {                                                                             \
      using type = decltype(DEFAULT_VALUE);                                       \
      static constexpr std::string_view KEY = #KEY_VAL "\0";                      \
      KEY_VAL##Map(CurrentFile& parent) : My{parent} { parent.Register(*this); }  \
                                                                                  \
      type value = DEFAULT_VALUE;                                                 \
      CurrentFile& My;                                                            \
      void Set(const std::string_view& inVal)                                     \
      {

#define END_KEYMAPPED_DECL(KEY)         \
       }                                \
    } m_##KEY{*this};                   \
  public:                               \
                                        \
    const KEY##Map::type& KEY () const  \
    {                                   

#define END_KEYMAPPED_WITH_CACHE(KEY) \
        needsCache = true;            \
      }                               \
      bool needsCache{true};          \
      volatile bool finalized{false}; \
      void Finalize()                 \
      {                               \
        if(needsCache)                \
        {                             \
          auto& cache = value;        \

#define END_KEYMAPPED_GET(KEY) \
      return m_##KEY.value;    \
    }                                                       

#define END_CACHED_GET(KEY)                                 \
        }                                                   \
        else if(!finalized)                                 \
        {                                                   \
          while(!finalized) {}                              \
        }                                                   \
                                                            \
      END_KEYMAPPED_DECL(KEY)                               \
      {                                                     \
        const_cast<CurrentFile*>(this)->m_##KEY.Finalize(); \
      }                                                     \
      return m_##KEY.value;                                 \
    }

#define END_KEYMAPPED_VAR(KEY)  \
    END_KEYMAPPED_DECL(KEY)     \
    END_KEYMAPPED_GET(KEY)

#define DEFAULT_KEYMAPPED_SET() \
  {                             \
    value = inVal;              \
  }

#define KEYMAPPED_VAR_DECL(KEY, DEFAULT_VALUE)  \
  NEW_KEYMAPPED_VAR(KEY, DEFAULT_VALUE)         \
  DEFAULT_KEYMAPPED_SET()                       \
  END_KEYMAPPED_DECL(KEY) 

#define KEYMAPPED_VAR_CACHED_DECL(KEY, DEFAULT_VALUE) \
  NEW_KEYMAPPED_VAR(KEY, DEFAULT_VALUE)               \
  DEFAULT_KEYMAPPED_SET()                             \
  END_KEYMAPPED_WITH_CACHE(KEY)

#define DERIVED_VALUE(KEY, DEFAULT_VALUE)                                                                     \
  NEW_KEYMAPPED_VAR(KEY, DEFAULT_VALUE)                                                                       \
  {                                                                                                           \
    throw std::runtime_error("Ignoring config argument " + std::string{inVal} + " for derived value " #KEY);  \
  }                                                                                                           \
  END_KEYMAPPED_WITH_CACHE(KEY)

#define DEFAULT_DERIVED(KEY)        \
  DERIVED_VALUE(KEY, std::string{})

#define KEYMAPPED_VAR(KEY, DEFAULT_VALUE)                   \
      NEW_KEYMAPPED_VAR(KEY, std::string{ DEFAULT_VALUE })  \
      DEFAULT_KEYMAPPED_SET()                               \
      END_KEYMAPPED_VAR(KEY)

#endif
}
